FROM node:10.11.0

#create dir
RUN mkdir -p /usr/src
WORKDIR /usr/src

# install dependences
ADD package.json /usr/src/package.json
RUN npm install

# Bundle srcs
COPY . /usr/src

EXPOSE 8888

CMD npm run dev