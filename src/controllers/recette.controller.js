const Recette = require('../models/recette.model');
const User = require('../models/user.model');

const create = async (ctx) => {
    const params = ctx.request.body;
    await _createRecette(params)
        .then(async (newRecette) => {
            await _updateUser(newRecette._id, params.userId)
                .then((user) => { ctx.ok(user) })
                .catch((err) => { ctx.internalServerError(err) });
            ctx.created(newRecette)
        })
        .catch((err) => { ctx.internalServerError(err) });
};

const _createRecette = async (params) => {
    return new Promise((resolve, reject) => {
        const newRecette = new Recette({
            title: params.title,
            content: params.content,
        });
        newRecette.save((err) => {
            if (err) {
                reject(err);
            } else {
                resolve(newRecette);
            }
        });
    });
};

const _updateUser = async (recetteId, userId) => {
    const user = await User.findOne({ _id: userId });
    const recettes = user.recettes;
    recettes.push(recetteId)
    return new Promise((resolve, reject) => {
        User.updateOne({ '_id': userId }, { 'recettes': recettes })
            .then((user) => resolve(user));
    });
};

const getAll = async (ctx) => {
    ctx.ok("ca va");
    // const params = ctx.request.body;
    // await _createUser(params)
    //     .then((newUser) => { ctx.created(newUser) })
    //     .catch((err) => { ctx.internalServerError(err) });

};

const getOne = async (ctx) => {
    const params = ctx.request.query;
    await _getOne(params.id)
        .then((recette) => {
            ctx.ok(recette)
        })
        .catch((err) => { ctx.internalServerError(err) });
};

const _getOne = async (id) => {
    const recette = await Recette.findOne({ _id: id });
    return new Promise((resolve, reject) => {
        if (recette)
            resolve(recette);
        else
            reject()
    });
};

module.exports = {
    create,
    getAll,
    getOne
};