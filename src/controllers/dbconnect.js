const mongoose = require('mongoose');

const connect = (uri) => {
	mongoose.connect(uri,{ useUnifiedTopology: true, useNewUrlParser: true }
	);
};

module.exports = {
	connect
};