const Recette = require('../models/recette.model');
const User = require('../models/user.model');


const signUp = async (ctx) => {
    const params = ctx.request.body;
    // voir si on check pour le nom d'utilisateur
    // alreadyRegistered = await _checkAlreadyRegistered(params.mail);
    // if (!alreadyRegistered)
        await _createUser(params)
            .then((newUser) => { ctx.created(newUser) })
            .catch((err) => { ctx.internalServerError(err) });
    // else
        // ctx.unauthorized();
};

const _checkAlreadyRegistered = async (pseudo) => {
    return (user = await User.findOne({ pseudo }) ? true : false);
};

const _createUser = async (params) => {
    return new Promise((resolve, reject) => {
        const newUser = new User({
            pseudo: params.pseudo,
            password: params.password,
            firstname: params.firstname,
            lastname: params.lastname,
            bio: "",
            comis: [],
            chefs: [],
            recettes: []
        });
        newUser.save((err) => {
            if (err) {
                reject(err);
            } else {
                resolve(newUser);
            }
        });
    });
};

const signIn = async (ctx) => {
    const user = await User.findOne({ mail: ctx.request.body.mail, password: ctx.request.body.password });
    if (user) {
        await _checkCredentials(ctx.request.body.pseudo, ctx.request.body.password)
            .then((res) => { res ? ctx.ok(res) : ctx.unauthorized(); })
            .catch((err) => { ctx.internalServerError(err); });
    } else {
        ctx.unauthorized();
        ctx.body = "Wrong mail/password";
    }
};

const _checkCredentials = async (pseudo, password) => {
    return new Promise((resolve, reject) => {
        User.findOne({
            pseudo,
            password
        }, (err, res) => {
            if (err)
                reject(err);
            else {
                resolve(res);
            }
        });
    });
};

const getProfile = async (ctx, params = null) => {
    console.log("ici", ctx.request.query.id)
    await _getProfile(ctx.request.query.id ? ctx.request.query.id : params.id )
            .then((user) => { user ? ctx.ok(user) : ctx.unauthorized(); })
            .catch((err) => { ctx.internalServerError(err); });
};

const _getProfile = async (_id) => {
    const user = await User.findOne({ _id: _id });
    return new Promise((resolve, reject) => {
        resolve(user);
    });
};

const search = async (ctx) => {
    await _search(ctx.request.query.pseudo)
        .then((users) => {console.log(users, users.length);ctx.ok(users)})
        .catch((err) => { ctx.internalServerError(err); });
};

const _search = async (pseudo) => {
    const users = User.find({ 'pseudo': { $regex: "^" + pseudo, $options: "i" } }, { genre: 0, metadata: 0, __v: 0 }).limit(20)
    return new Promise((resolve, reject) => {
        resolve(users);
    });
};

const follow = async (ctx) => {
    const params = ctx.request.body;
    await _toFollow(params.userId, params.toFollowId)
        .then((user) => {ctx.ok(user)})
        .catch((err) => { ctx.internalServerError(err); });
    await _follow(params.userId, params.toFollowId)
        .then((user) => {ctx.ok(user)})
        .catch((err) => { ctx.internalServerError(err); });
};

const _toFollow = async (userId, toFollowId) => {
    const user = await User.findOne({ '_id': toFollowId});
    const comis = user.comis;
    comis.push(userId);
    return new Promise((resolve, reject) => {
        User.updateOne({ '_id': toFollowId }, { 'comis': comis })
            .then((user) => resolve(user));
    });
};

const _follow = async (userId, toFollowId) => {
    const user = await User.findOne({ '_id': userId });
    const chefs = user.chefs;
    chefs.push(toFollowId);
    console.log(userId, toFollowId)
    return new Promise((resolve, reject) => {
        User.updateOne({ _id: userId }, { chefs: chefs })
            .then((user) => resolve(user));
    });
};

const unfollow = async (ctx) => {
    const params = ctx.request.body;
    await _toUnFollow(params.userId, params.toFollowId)
        .then((user) => {ctx.ok(user)})
        .catch((err) => { ctx.internalServerError(err); });
    await _unFollow(params.userId, params.toFollowId)
        .then((user) => {ctx.ok(user)})
        .catch((err) => { ctx.internalServerError(err); });
};

const _toUnFollow = async (userId, toFollowId) => {
    const user = await User.findOne({ '_id': toFollowId});
    const comis = user.comis;
    const index = comis.indexOf(userId);
    comis.splice(index, 1);
    return new Promise((resolve, reject) => {
        User.updateOne({ '_id': toFollowId }, { 'comis': comis })
            .then((user) => resolve(user));
    });
};

const _unFollow = async (userId, toFollowId) => {
    const user = await User.findOne({ '_id': userId });
    const chefs = user.chefs;
    const index = chefs.indexOf(toFollowId)
    chefs.splice(toFollowId, 1);
    return new Promise((resolve, reject) => {
        User.updateOne({ _id: userId }, { chefs: chefs })
            .then((user) => resolve(user));
    });
};

const feed = async (ctx) => {
    console.log('here')
    await _feed(ctx.request.query.id)
        .then((feed) => { console.log('end', feed);ctx.ok(feed)})
        .catch((err) => { ctx.internalServerError(err); });
};

const _feed = (userId) => {
    console.log('here 2', userId)
    const user =  User.findOne({ '_id': userId });
    console.log('here 3', user)
    const following = user.chefs;
    console.log('here 4', following)
    const ids = []
    for (let index = 0; index < following.length; index++) {
        console.log('4.25', following[index])
        const getId =  User.findOne({ '_id': following[index] })
        console.log('4.5', getId);
        console.log('4.5', getId.recettes);
        for (let i = 0; i < getId.recettes.length; i++) {
            ids.push(getId.recettes[i]);
        }
    }
    const recettes = [];
     following.map(async (chef) => {
        recettes.push(toto(chef, recettes));
        return new Promise((resolve, reject) => {
            console.log(' 6', recettes)
                resolve(recettes);
            });
    })
    
};

const toto = async (chef) => {
    const recettes = [];
    const getId = await User.findOne({ _id: chef })
    console.log('5', getId.recettes.length);
    for (let i = 0; i < getId.recettes.length; i++) {
        console.log('5.5', getId.recettes[i]);
        recettes.push(getId.recettes[i]);
    }
    return new Promise((resolve) => {
        resolve(recettes);
    });
};

const health = async (ctx) => {
    ctx.ok('On est uuuuuuup!');
};

const updateProfile = async (ctx) => {
    const params = ctx.request.body;
    __updateProfile(params)
        .then(() => {
            ctx.ok();
        }) 
        .catch((err) => { ctx.internalServerError(err); });
};

const __updateProfile = async (params) => {
    console.log(params);
    return new Promise((resolve, reject) => {
        resolve(User.updateOne({ 
            '_id': params.id
        }, {
            pseudo: params.pseudo, 
            firstname: params.firstname,
            lastname: params.lastname, 
            bio: params.bio
        }));
    });
};

module.exports = {
    signUp,
    signIn,
    getProfile,
    search,
    follow,
    unfollow,
    feed,
    health,
    updateProfile
};