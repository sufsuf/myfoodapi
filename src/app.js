require('dotenv').config({ path: `${__dirname}/.env` });
const Koa = require('koa');
const respond = require('koa-respond');
const bodyParser = require('koa-bodyparser');

const db = require('./controllers/dbconnect');

const authRoutes = require('./routes/login.route');
const recetteRoutes = require('./routes/recette.route');
const userRoutes = require('./routes/user.route');

const app = new Koa();
const port = 8888 || 5000;

db.connect(`mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASSWORD}@myfood.o2lwv.mongodb.net/MyFood`);

app.use(bodyParser());
app.use(respond());

app.use(authRoutes);
app.use(recetteRoutes);
app.use(userRoutes);

const server = app.listen(port, () => {
  console.log(`Server MyFood listening on port ${port}`);
});
module.exports = { server };