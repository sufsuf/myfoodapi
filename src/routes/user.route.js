const Router = require('koa-router');
const user = require('../controllers/user.controller');

const router = new Router();

router.patch('/profile', user.updateProfile)
router.get('/profile', user.getProfile);
router.get('/user/search', user.search);
router.post('/user/follow', user.follow);
router.post('/user/unfollow', user.unfollow);
router.get('/feed', user.feed);
router.get('/health', user.health);

module.exports = router.routes();