const Router = require('koa-router');
const recette = require('../controllers/recette.controller');

const router = new Router();

router.post("/recette", recette.create);
router.get("/recette", recette.getOne);

router.get("/recettes", recette.getAll);

module.exports = router.routes();