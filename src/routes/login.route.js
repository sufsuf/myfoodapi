const Router = require('koa-router');
const user = require('../controllers/user.controller');

const router = new Router();

router.post("/signin", user.signIn);
router.post("/signup", user.signUp);

module.exports = router.routes();