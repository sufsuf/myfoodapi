const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema ({
    pseudo: String,
    password: String,
    firstname: String,
    lastname: String,
    bio: String,
    comis: [String],
    chefs: [String],
    recettes: [String]
});

mongoose.model('User', userSchema);

module.exports = mongoose.model('User');