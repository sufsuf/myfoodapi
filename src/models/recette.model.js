const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const recetteSchema = new Schema ({
    title: String,
    content: String
});

mongoose.model('Recette', recetteSchema);

module.exports = mongoose.model('Recette');